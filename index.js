
// Set used var
const expressLib = require('express');
const webApp = expressLib();
const listenPort = 8080;

var header ="<p><b>Ma page web par node js</b></p>"


// '/' is page call ('/' for default page)
webApp.get('/', function (req, res) {
  res.send(header+'Bonjour le monde!');
  });


// Adapt listen port for GCP  
webApp.listen(listenPort, function () {
  console.log("L'application écoute le port "+listenPort+"!");
  // Ou: console.log(`L'application écoute le port ${listenPort}!`);
  });

